package com.example.natpctask.repository;

import static org.assertj.core.api.Assertions.assertThat;

import com.example.natpctask.Repository.AppUserRepository;
import com.example.natpctask.model.AppUser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class AppUserRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private AppUserRepository repository;

    @Test
    public void shouldGetAdminUser() throws  Exception{
        AppUser user = repository.getOne(1L);
        assertThat(user.getPassword()).isEqualTo("admin");
        assertThat(user.getLogin()).isEqualTo("admin");
        assertThat(user.getName()).isEqualTo("admin");
    }

    @Test
    public void testFindAll(){
        AppUser firstUser = new AppUser("FirstName","FirstLogin","FirstPass");
        entityManager.persist(firstUser);
        entityManager.flush();

        AppUser secondUser= new AppUser("SecondName","SecondLogin","SecondPass");
        entityManager.persist(secondUser);
        entityManager.flush();

        List<AppUser> appUsers = repository.findAll();

        //three because there is admin user already + 2 above
        assertThat(appUsers.size()).isEqualTo(3);
        assertThat(appUsers.get(1)).isEqualTo(firstUser);
        assertThat(appUsers.get(2)).isEqualTo(secondUser);
    }

    @Test
    public void testFindByLogin(){
        AppUser firstUser = new AppUser("FirstName","FirstLogin","FirstPass");
        entityManager.persist(firstUser);
        entityManager.flush();

        Optional<AppUser> testUser = repository.findByLogin(firstUser.getLogin());
    }
}
