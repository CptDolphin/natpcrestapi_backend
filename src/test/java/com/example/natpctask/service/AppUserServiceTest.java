package com.example.natpctask.service;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.junit.Assert.assertEquals;
import static org.assertj.core.api.Assertions.assertThat;

import com.example.natpctask.Repository.AppUserRepository;
import com.example.natpctask.model.AppUser;
import javassist.NotFoundException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class AppUserServiceTest {
    public static final String HTTP_GET_ALL = "http://localhost:8080/users/getAll";

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Autowired
    private AppUserService appUserService;

    @MockBean
    private AppUserRepository appUserRepository;

    private AppUser appUser;

    @Before
    public void setUp() {
        appUser = new AppUser("testlogin2", "tesname2", "testpassword2");
        appUser.setId(2L);

        when(appUserRepository.findByLogin(appUser.getLogin()))
                .thenReturn(Optional.of(appUser));
    }

    @Test
    public void contextLoads() {
        assertThat(appUserService).isNotNull();
    }

    @Test
    public void shouldReturnAppUserById() {
        Optional<AppUser> result = appUserService.getAppUserWithId(appUser.getId());
        if (result.isPresent()) {
            AppUser user = result.get();
            assertEquals(user, result);
        }
    }


}

