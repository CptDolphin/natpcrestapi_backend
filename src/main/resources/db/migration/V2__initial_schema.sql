CREATE TABLE app_user(
  id INT PRIMARY KEY AUTO_INCREMENT,
  login VARCHAR(256) NOT NULL,
  name VARCHAR(256) NOT NULL,
  password VARCHAR(256) NOT NULL
);

CREATE TABLE contact(
  id INT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(256) NOT NULL,
  surname VARCHAR(256) NOT NULL,
  email VARCHAR(256) NOT NULL UNIQUE,
  password VARCHAR(256) NOT NULL,
  telephone VARCHAR(256) NOT NULL UNIQUE,
  birthdate DATE NOT NULL,
  category VARCHAR(256)
);

INSERT INTO app_user (login,name,password) VALUES ("admin","admin","admin");
INSERT INTO contact (name,surname,email,password,telephone,birthdate,category) VALUES ("name1","surname1","email1@gmail.com","password1","4501501","2018-04-24","category1");