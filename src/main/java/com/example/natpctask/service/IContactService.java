package com.example.natpctask.service;

import com.example.natpctask.model.Contact;

import java.util.Optional;

public interface IContactService {
    Optional<Contact> createContact(Contact contact);
    Optional<Contact> updateContact(Long id, Contact contact);
}
