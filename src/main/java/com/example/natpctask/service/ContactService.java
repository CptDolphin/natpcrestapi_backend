package com.example.natpctask.service;

import com.example.natpctask.Repository.ContactRepository;
import com.example.natpctask.exceptions.ContactIsInvalidException;
import com.example.natpctask.exceptions.ContactWithThatEmailExistsException;
import com.example.natpctask.exceptions.ContactWithThatTelephoneExistsException;
import com.example.natpctask.exceptions.NotFoundException;
import com.example.natpctask.model.Contact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ContactService {

    @Autowired
    private ContactRepository repository;

    public Optional<Contact> createContact(Contact contact) {
        System.out.println("inside service add ");
        if (isContactInvalid(contact)) {
            throw new ContactIsInvalidException();
        }
        Contact contactToAdd = repository.save(contact);
        return Optional.ofNullable(contactToAdd);
    }

    private boolean isContactEmailExisting(Contact contact) {
        return repository.findByEmail(contact.getEmail()).isPresent();
    }

    private boolean isContactTelephoneExisting(Contact contact) {
        return repository.findByTelephone(contact.getTelephone()).isPresent();
    }


    private boolean isContactInvalid(Contact contact) {
        if (isContactEmailExisting(contact)) {
            throw new ContactWithThatEmailExistsException();
        }
        if(isContactTelephoneExisting(contact)){
            throw new ContactWithThatTelephoneExistsException();
        }
        return contact.getEmail() == null ||
                contact.getEmail().isEmpty() ||
                contact.getTelephone() == null ||
                contact.getTelephone().isEmpty();
    }

    public Optional<Contact> updateContact(Long id, Contact contact) {
        Optional<Contact> optionalContact = repository.findById(id);
        if(optionalContact.isPresent()){
            deleteContact(id);
            contact.setId(id);
            return Optional.ofNullable(contact);
        }
        return Optional.empty();
    }

    public List<Contact> getAllContacts() {
        System.out.println("inside service getAll");
        return repository.findAll();
    }

    public Contact getContactWithId(Long id) {
        return repository.findById(id)
                .orElseThrow(() ->
                        new NotFoundException(String.format("Product with id %s not found", id)));
    }

    public void deleteContact(Long id) {
        repository.delete(getContactWithId(id));
    }
}
