package com.example.natpctask.service;

import com.example.natpctask.model.AppUser;
import com.example.natpctask.model.dto.AppUserDto;
import com.example.natpctask.model.dto.AppUserRegisterDto;
import com.example.natpctask.model.dto.LoginDto;

import java.util.List;
import java.util.Optional;

public interface IAppUserService {
    Optional<AppUserDto> registerUser(AppUserRegisterDto user);
    Optional<AppUser> getAppUserWithId(Long id);
    List<AppUser> getAllUsers();
    Optional<AppUser> getAppUserWithLogin(String login);
    Optional<AppUser> getUserWithLoginAndPassword(LoginDto dto);
}
