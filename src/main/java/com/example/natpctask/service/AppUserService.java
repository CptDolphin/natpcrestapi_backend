package com.example.natpctask.service;

import com.example.natpctask.Repository.AppUserRepository;
import com.example.natpctask.exceptions.UserIsInvalidException;
import com.example.natpctask.exceptions.UserWithThatLoginExistsException;
import com.example.natpctask.model.AppUser;
import com.example.natpctask.model.dto.AppUserDto;
import com.example.natpctask.model.dto.AppUserRegisterDto;
import com.example.natpctask.model.dto.LoginDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AppUserService implements IAppUserService {

    @Autowired
    private AppUserRepository appUserRepository;

    @Override
    public Optional<AppUserDto> registerUser(AppUserRegisterDto user) {
        System.out.println("Process to register user started, user:" + user.toString());
        if (isInvalidUser(user)) {
            throw new UserIsInvalidException();
        }
        if (isLoginRegistered(user.getLogin())) {
            throw new UserWithThatLoginExistsException();
        }
        AppUser userToRegister = new AppUser(user.getLogin(), user.getName(), user.getPassword());
//        userToRegister.setPassword(bCryptPasswordEncoder.encode(userToRegister.getPassword()));
        appUserRepository.save(userToRegister);
        return Optional.ofNullable(AppUserDto.create(userToRegister));
    }

    @Override
    public Optional<AppUser> getAppUserWithId(Long id) {
        return appUserRepository.findById(id);
    }

    @Override
    public List<AppUser> getAllUsers() {
        return appUserRepository.findAll();
    }

    @Override
    public Optional<AppUser> getAppUserWithLogin(String login) {
        return appUserRepository.findByLogin(login);
    }

    @Override
    public Optional<AppUser> getUserWithLoginAndPassword(LoginDto dto){
        Optional<AppUser> userFromDB = appUserRepository.findByLogin(dto.getLogin());
        if (userFromDB.isPresent()) {
//            AppUser user = userFromDB.get();
//            if (bCryptPasswordEncoder.matches(dto.getPassword(), user.getPassword())) {
//            }
            return userFromDB;
        }
        return Optional.empty();
    }


    public boolean isLoginRegistered(String login) {
        return appUserRepository.findByLogin(login).isPresent();
    }

    private boolean isInvalidUser(AppUserRegisterDto user) {
        return user.getLogin() == null ||
                user.getLogin().isEmpty() ||
                user.getPassword() == null ||
                user.getPassword().isEmpty();
    }
}
