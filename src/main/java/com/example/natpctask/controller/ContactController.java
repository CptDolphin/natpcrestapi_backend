package com.example.natpctask.controller;

import com.example.natpctask.exceptions.ContactIsInvalidException;
import com.example.natpctask.exceptions.ContactWithThatEmailExistsException;
import com.example.natpctask.exceptions.ContactWithThatTelephoneExistsException;
import com.example.natpctask.model.AppUser;
import com.example.natpctask.model.Contact;
import com.example.natpctask.response.ResponseFactory;
import com.example.natpctask.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/contacts")
public class ContactController {

    @Autowired
    private ContactService contactService;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Contact> addContact(@RequestBody Contact contact) {
        System.out.println("add inuitialized");
        Optional<Contact> optionalContact = contactService.createContact(contact);
        if (optionalContact.isPresent()) {
        System.out.println("inside if ");
            return ResponseFactory.ok(optionalContact.get());
        }
        return ResponseFactory.badRequest();
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<Contact> updateContact(@RequestParam("id") Long id, @RequestBody @Valid Contact contact) {
        Optional<Contact> contactToUpdate = contactService.updateContact(id, contact);
//        return ResponseFactory.ok(contactToUpdate.get());
        return contactToUpdate.map(ResponseFactory::ok).orElseGet(ResponseFactory::badRequest);

    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Contact> getAppUser(@RequestParam(name = "id") Long id) {
        return ResponseFactory.ok(contactService.getContactWithId(id));
    }

    @RequestMapping(path = "/{id}",method = RequestMethod.GET)
    public ResponseEntity<Contact> getAppUserWithId(@PathVariable("id") Long id) {
        return ResponseFactory.ok(contactService.getContactWithId(id));
//        Optional<Contact> contact = contactService.getContactWithId(id);
//        return contact.map(ResponseFactory::ok).orElseGet(ResponseFactory::badRequest);
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Contact>> getAll() {
        System.out.println("inside controller getAll");
        List<Contact> contacts = contactService.getAllContacts();
        return ResponseFactory.ok(contacts);
    }

    @RequestMapping(path = "/{id}",method = RequestMethod.DELETE)
    public void deleteContact(@PathVariable("id")Long id){
        contactService.deleteContact(id);
    }

}
