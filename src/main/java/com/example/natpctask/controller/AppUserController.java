package com.example.natpctask.controller;

import com.example.natpctask.exceptions.UserErrorException;
import com.example.natpctask.model.AppUser;
import com.example.natpctask.model.dto.AppUserDto;
import com.example.natpctask.model.dto.AppUserRegisterDto;
import com.example.natpctask.response.ResponseFactory;
import com.example.natpctask.service.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/users")
public class AppUserController {

    @Autowired
    private AppUserService appUserService;

    @RequestMapping(path = "/add", method = RequestMethod.POST)
    public ResponseEntity<AppUserDto> addUser(@RequestBody AppUserRegisterDto user) {
        System.out.println("add method initialized");
        try {
            Optional<AppUserDto> registerUser = appUserService.registerUser(user);
            if (registerUser.isPresent()) {
                return ResponseFactory.created(registerUser.get());
            }
        } catch (UserErrorException ex) {
            ex.printStackTrace();
        }
        return ResponseFactory.badRequest();
    }

    @RequestMapping(path = "/get", method = RequestMethod.GET)
    public ResponseEntity<AppUser> getAppUser(@RequestParam(name = "id") Long id) {
        System.out.println("get method initialized");
        Optional<AppUser> user = appUserService.getAppUserWithId(id);
        return user.map(ResponseFactory::ok).orElseGet(ResponseFactory::badRequest);
//        return ResponseFactory.ok(new AppUser("random","random","random"));
    }

    @RequestMapping(path = "/get/{id}", method = RequestMethod.GET)
    public ResponseEntity<AppUser> getAppUserWithId(@PathVariable("id") Long id) {
        System.out.println("get/{id} method initialized");
        Optional<AppUser> user = appUserService.getAppUserWithId(id);
        return user.map(ResponseFactory::ok).orElseGet(ResponseFactory::badRequest);
    }

    @RequestMapping(path = "/getAll", method = RequestMethod.GET)
    public ResponseEntity<List<AppUser>> getAll() {
        System.out.println("getAll method initialized");
        List<AppUser> users = appUserService.getAllUsers();
        return ResponseFactory.ok(users);
    }
}
