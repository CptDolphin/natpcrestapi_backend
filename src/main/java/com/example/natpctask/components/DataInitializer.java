package com.example.natpctask.components;

import com.example.natpctask.model.dto.AppUserRegisterDto;
import com.example.natpctask.service.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DataInitializer {

    @Autowired
    private AppUserService appUserService;

    @Autowired
    public DataInitializer(AppUserService appUserService) {
        this.appUserService = appUserService;
        initialize();
    }

// only used when not manualy writing db
// and ddl-auto=createContact to initialize admin admin user account
// dont if using already existint db ( V2__initial_schema.sql )
    private void initialize(){
//        if(!appUserService.getAppUserWithLogin("admin").isPresent()){
//            appUserService.registerUser(new AppUserRegisterDto("admin","admin","admin"));
//        }
    }
}
