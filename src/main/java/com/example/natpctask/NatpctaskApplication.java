package com.example.natpctask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationStrategy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

@SpringBootApplication
public class NatpctaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(NatpctaskApplication.class, args);
    }

    //every project run database is cleaned and re-run
    // by default admin admin user avaible
    @Bean
    @Profile("!production")
    public FlywayMigrationStrategy cleanMigrationStrategy() {
        return flyway -> {
            flyway.clean();
            flyway.migrate();
        };
    }
}
