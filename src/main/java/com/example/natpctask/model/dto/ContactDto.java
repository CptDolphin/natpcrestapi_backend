//package com.example.natpctask.model.dto;
//
//import com.example.natpctask.model.Contact;
//import lombok.AllArgsConstructor;
//import lombok.Getter;
//import lombok.NoArgsConstructor;
//import lombok.Setter;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.validation.constraints.Size;
//import java.util.Date;
//
//@Getter
//@Setter
//@NoArgsConstructor
//@AllArgsConstructor
//public class ContactDto {
//
//    private String name;
//    private String surname;
//    @Column(unique = true, nullable = false)
//    private String email;
//    @Size(min = 5,max = 256)
//    @Column(nullable = false)
//    private String password;
//    @Column(unique = true, nullable = false)
//    private String telephone;
//    private String category;
//
//    public static ContactDto create(Contact contact){
//        return new ContactDto(
//                contact.getName(),
//                contact.getSurname(),
//                contact.getEmail(),
//                contact.getPassword(),
//                contact.getTelephone(),
//                contact.getCategory()
//        );
//    }
//
//}
