package com.example.natpctask.model.dto;

import com.example.natpctask.model.AppUser;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AppUserDto {
    private Long id;
    private String login;
    private String name;

    public static AppUserDto create(AppUser user){
        return new AppUserDto(
                user.getId(),
                user.getLogin(),
                user.getName());
    }
}
