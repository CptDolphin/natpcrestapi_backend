package com.example.natpctask.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.repository.Temporal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.Instant;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Contact extends BaseEntity{

    private String name;
    private String surname;

    @Column(unique = true, nullable = false)
    private String email;

    @Size(min = 5,max = 256)
    @Column(nullable = false)
    private String password;

    //shouldn't store phone numbers as numbers in db
    // cause it might cut 0s and other undesirable things
    // "+" instead of 00 in international numbers etc.
    @Column(unique = true, nullable = false)
    private String telephone;

    @Column(nullable = false)
//    private Date myDate;
    private Date birthdate;

    private String category;

    public Contact(String name, String surname, String email, @Size(min = 5, max = 256) String password, String telephone, String category) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
        this.telephone = telephone;
        this.birthdate = Date.from(Instant.now());
        this.category = category;
    }
}
